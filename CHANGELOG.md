Hotfix 10/04/17:

* Fix bug with requirement for diapering being a little, instead of a big.

Update 10/04/17:

* Capped Skill levels and Incontinence at 10 and 100.
* Diaperfullness stat is reset upon being changed.
* User messages are now censored or lisped if their regression level is too high or they have an item equipped that limits their speech
* Webhook integration with Discord to display "edited" messages as the original users instead of the bot
* !change command now allows you to specify thickness of the new diaper
* listPlayers no longer mentions everybody
* Can now request the stats of another player with !stats playername
* Fixed some spacing issues in status messages
* Fixed some breaking issues in the "pause" logic
