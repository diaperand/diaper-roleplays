# Contributing

dndrp is programming in the Go programming language (Golang). If you want to help out you will need to know it. 

Golang is a simple, but powerful concurrent language with similar syntax to C. If you don't know Golang but would like to learn, I recommend checking out the language's [documentation](https://golang.org/doc/).

# Other ways to contribute

If you don't have any coding knowledge, that's fine! You can still help out in other ways.

For instance, the bot replies in various different ways to users, picking from a random list of replies. That list can be found in the [statusmsgs.json](https://gitlab.com/diaperand/diaper-roleplays/blob/master/statusmsgs.json) file.

More non-coding ways to contribute will be coming in the future, so stay tuned!
