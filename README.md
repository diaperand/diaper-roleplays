# Diaper Roleplay Bot

[![GoDoc](https://godoc.org/gitlab.com/diaperand/diaper-roleplays?status.svg)](https://godoc.org/gitlab.com/diaperand/diaper-roleplays) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/diaperand/diaper-roleplays)](https://goreportcard.com/report/gitlab.com/diaperand/diaper-roleplays)

A matrix bot that allows groups of players to have a Dungeon & Dragons style roleplay that's filled to the brim with diaper-related themantics.

## Forward Notes

Only self-hosting the bot is supported. To do so, you must first be familiar with Matrix, the extensible, federated chat protocol: https://matrix.org

If this is your first time hearing of such a thing, I recommend you make a quick account on [Riot.im](https://riot.im/app) (no email or phone needed) and hop into one of the suggested rooms (like [#MatrixHQ](https://riot.im/app/#/room/#matrix:matrix.org) or [#Offtopic](https://riot.im/app/#/room/#offtopic:matrix.org)). Riot is essentially an open-source slack/Discord alternative that uses the Matrix protocol.

Right, so now you should be at least somewhat familiar with Matrix and what it's all about. Let's get to installing the bot.

## Building

First follow the [instructions](https://github.com/matrix-org/go-neb) on installing Go-NEB, the official Matrix bot framework.

Once Go-NEB is installed and working, place the contents of this repo into `go-neb/src/github.com/matrix-org/services/` then in the `go-neb` directory, run:

```
gb vendor restore
```

 to download the bot's dependencies. Then:

```
gb build
```

to build the source.

## Running

After setup, you should be able to run the bot with the following command (make sure to replace `https://public.facing.endpoint` with the public domain of a Matrix home server):

```
BIND_ADDRESS=:4050 DATABASE_TYPE=sqlite3 CONFIG_URL=config.yaml BASE_URL=https://public.facing.endpoint bin/go-neb
```
