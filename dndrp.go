// Package dndrp implements a Service that acts as a helpful RPG bot for
// DnD-style RPs
package dndrp

/*
TODO:

*/

import (
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/matrix-org/go-neb/types"
	"github.com/matrix-org/gomatrix"
)

var (
	paused_players map[string]*Player
	players        map[string]*Player
	items          map[string]*Item
	statusMsgs     map[string]*StatusMsg
	avatars        map[string]string
	service        *Service

	wordReplacements map[string][]string

	discord        *discordgo.Session
	registrations  []*DiscordMessage
	discordMessages map[string]*discordgo.Message

	token          string
	hsURL		string
)

// Configure service type
const ServiceType = "dndrp"

// TODO: Add config fields?
type Service struct {
	types.DefaultService
}

type StatusMsg struct {
	Description string   `json:"description"`
	StatusTexts []string `json:"statustexts"`
}

type DiscordMessage struct {
	ID	string
	Content string
	Channel string
}

// getStatus grabs a random status string from the statusmsgs.json file and
// returns it as a message for the bot to say to the user
func getStatus(statusId string) string {
	// Get the correct StatusMsg object
	msg, ok := statusMsgs[statusId]

	if !ok {
		return fmt.Sprintln("Unknown status identifier: ", statusId, statusMsgs)
	}

	// Generate a random int within range
	rand.Seed(time.Now().Unix())
	index := rand.Intn(len(msg.StatusTexts))

	return msg.StatusTexts[index]
}

// Stats define a series of attributes that a player can have.
// They can also be modified by items or spells
// TODO: Be able to influence a player's stats in a simple format:
// !ch playername st:+1 in:-5
type Stats struct {
	Strength     int `json:"strength"`
	Constitution int `json:"constitution"`
	Dexterity    int `json:"dexterity"`
	Intelligence int `json:"intelligence"`
	Wisdom       int `json:"wisdom"`
	Charisma     int `json:"charisma"`
	Regression   int `json:"regression"`
	Incontinence int `json:"incontinence"`

	Bladder int `json:"bladder"`
	Bowels  int `json:"bowels"`
}

// limitTo is a helper function to limit a value to a particular maximum.
// There's probably a built in for this somewhere...
func limitTo(number, maxValue int) int {
	if number > maxValue {
		number = maxValue
	}

	return number
}

func (dst *Stats) addStats(src *Stats) interface{} {
	dst.Strength += src.Strength
	dst.Constitution += src.Constitution
	dst.Dexterity += src.Dexterity
	dst.Intelligence += src.Intelligence
	dst.Wisdom += src.Wisdom
	dst.Charisma += src.Charisma
	dst.Regression += src.Regression
	dst.Incontinence += src.Incontinence

	dst.Bladder += src.Bladder
	dst.Bowels += src.Bowels

	// Limit values to their maximum
	maxSkillLevel := 10 // TODO: Make a part of config
	dst.Strength = limitTo(dst.Strength, maxSkillLevel)
	dst.Constitution = limitTo(dst.Constitution, maxSkillLevel)
	dst.Dexterity = limitTo(dst.Dexterity, maxSkillLevel)
	dst.Intelligence = limitTo(dst.Wisdom, maxSkillLevel)
	dst.Charisma = limitTo(dst.Regression, maxSkillLevel)
	dst.Incontinence = limitTo(dst.Incontinence, 100)

	return nil
}

// Stats for each player
type Player struct {
	Name      string   `json:"name"`
	RoomId    string   `json:"roomid"`
	ChannelId string   `json:"channelid"`
	Items     []string `json:"items"`
	AvatarURL string   `json:"avatarurl"`

	BladderWarningGiven bool      `json:"bladderwarninggiven"`
	BowelWarningGiven   bool      `json:"bowelwarninggiven"`
	Diapers             []*Diaper `json:"diapers"`
	DiaperFullness      int       `json:"diaperfullness"`

	HoldCount int `json:"holdcount"`
	MaxHold   int `json:"maxhold"`

	Stats *Stats `json:"stats"`

	IsBig bool `json:"isbig"`

	GroupId []string `json:"groupid"`

	// TODO: Integrate with an eqiup system, where the mouth component has a "mute amount"
	// Normal pacifier would be like 50%, whereas a ball gag would be 100%
	IsMuted bool `json:"ismuted"`
}

// GetItems is a helper function that returns a slice of Item objects that
// the player has access to
func (p *Player) GetItems(itemSlice []*Item) {
	for _, itemname := range p.Items {
		itemSlice = append(itemSlice, items[p.RoomId+itemname])
	}

	return
}

// RemoveItem is a helper function that removes a single instance of an item from
// a player's item list given only an item name
func (p *Player) RemoveItem(itemname string) {
	for index, value := range p.Items {
		if value == itemname {
			// Remove value at specific index from slice
			p.Items = append(p.Items[:index], p.Items[index+1:]...)
			return
		}
	}
}

// TODO: React to nickname updates

// DiaperThickness is a helper function that returns the total thickness of all
// of the given player's diapers as a sum representing the amount of
// centimeters
func (p Player) DiaperThickness() int {
	var totalThickness int = 0
	for _, v := range p.Diapers {
		totalThickness += v.Thickness
	}
	return totalThickness
}

// Diaper is a helper function that puts a diaper on a player
func (p *Player) Diaper(diaper *Diaper) interface{} {
	p.Diapers = append(p.Diapers, diaper)
	return message(p.Name, " is now wearing ", len(p.Diapers), " diaper(s) for ", p.DiaperThickness(), "cm of total padding!")
}

// increaseBladderBowels function augments the player's bladder and bowel
// levels while also warning them if they get too high.
func (p *Player) increaseBladderBowels() interface{} {
	fullMsg := ""

	// If player is still holding, then ignore
	if p.HoldCount > 0 {
		p.HoldCount -= 1

		if p.HoldCount == 0 {
			fullMsg += fmt.Sprintln(p.Name, getStatus("stopped_holding"))
		}
		return nil
	}

	p.Stats.Bladder += 3
	p.Stats.Bowels += 1

	wetMessThreshold := 100 - p.Stats.Incontinence/2
	warningThreshold := wetMessThreshold - 10

	rand.Seed(time.Now().Unix())
	randNum := rand.Intn(100)
	// Warn depending on random number
	shouldWarn := randNum > p.Stats.Incontinence

	if p.Stats.Bladder > wetMessThreshold {
		p.Stats.Bladder = 0
		p.BladderWarningGiven = false
		fullMsg += fmt.Sprintln(p.Name, getStatus("player_wet"))
	}
	if p.Stats.Bowels > wetMessThreshold {
		p.Stats.Bowels = 0
		p.BowelWarningGiven = false
		fullMsg += fmt.Sprintln(p.Name, getStatus("player_mess"))
	}

	if !p.BladderWarningGiven &&
		p.Stats.Bladder > warningThreshold {
		p.BladderWarningGiven = true
		if shouldWarn {
			fullMsg += fmt.Sprintln(p.Name, getStatus("player_wet_warn"))
		}
	}
	if !p.BowelWarningGiven &&
		p.Stats.Bowels > warningThreshold {
		p.BowelWarningGiven = true
		if shouldWarn {
			fullMsg += fmt.Sprintln(p.Name, getStatus("player_mess_warn"))
		}
	}

	if fullMsg != "" {
		return fullMsg
	}

	return nil
}

// TODO: Temporary stats

// ConsumeItem will remove an item from a player's inventory and add the item's
// attributes to the player's own attributes
func (p *Player) ConsumeItem(item *Item) interface{} {
	if err := p.Stats.addStats(item.StatMods); err != nil {
		return message("Error: ", err)
	}
	return message(p.Name, " consumed the ", item.Name, "!")
}

// WetDiaper will empty the contents of a player's bowels into their diaper
func (p *Player) WetDiaper() {
	// Empty their bladder into their diaper
	p.DiaperFullness += p.Stats.Bladder

	// Mark their bladder as empty
	p.Stats.Bladder = 0

	// Give them a couple holds
	p.MaxHold += 2
}

// MessDiaper will empty the contents of a player's bowels into their diaper
func (p *Player) MessDiaper() {
	// Empty their bowels into their diaper
	p.DiaperFullness += p.Stats.Bowels

	// Mark their bowels as empty
	p.Stats.Bowels = 0

	// Give them a couple holds
	p.MaxHold += 2
}

// Hold will hold in a players mess for a certain amount of time.
func (p *Player) Hold() {
	p.HoldCount += p.MaxHold
	p.MaxHold -= 1
}

// Change will change the player back into just a single, thin diaper
func (p *Player) Change(newThickness int) interface{} {
	// Remove existing diapers
	p.Diapers = nil

	// Add a new 1cm diaper
	newDiaper := &Diaper{
		Thickness: newThickness,
	}
	p.Diaper(newDiaper)

	p.DiaperFullness = 0

	return message(p.Name, getStatus("player_changed"))
}

// ListItems will return a formatted message that lists all currently known
// items for a specific user
func (p *Player) ListItems() interface{} {
	if len(p.Items) <= 0 {
		return message(p.Name, " has no items!")
	}

	fstr := "<p>Items for " + p.Name + ":</p><pre><code>"
	str := "Items for " + p.Name + ":\n```"
	first := true
	for _, itemName := range p.Items {
		if item, ok := items[p.RoomId+itemName]; ok {
			// Eliminate extra spacing on Riot
			if first {
				fstr = fstr + item.Name
				first = false
			} else {
				fstr = fstr + "\n" + item.Name
			}
			str = str + "\n" + item.Name
		}
	}
	fstr = fstr + "\n</code>"
	str = str + "\n```"
	return hmessage(str, fstr)
}

// getPlayerByRoomAndName returns a pointer to the player object given a roomId
// and player name
func getPlayerByRoomAndName(roomId, name string) (*Player, bool) {
	for _, player := range players {
		if player.RoomId == roomId && player.Name == name {
			return player, true
		}
	}

	// Error, couldn't find player
	return nil, false
}

// registerPlayer will add a new player, as well as ensure that they specified
// whether they want to be a big or a little
func registerPlayer(cli *gomatrix.Client, roomId, userId, displayName string, messageText []string) interface{} {
	if len(messageText) < 1 {
		return message("Syntax is !play big|little.")
	}
	if messageText[0] != "big" && messageText[0] != "little" {
		return message("You must specify if you're a big or a little. ",
			"Syntax is !play big|little")
	}
	if _, ok := players[roomId+userId]; ok {
		return message(getStatus("player_already_playing"))
	}
	if _, ok := paused_players[roomId+userId]; ok {
		return message(getStatus("player_already_playing") + " You are currently paused.")
	}

	isbig := messageText[0] == "big"

	// Grab base64 representation of avatar URL
	avatarURL := ""
	if discord != nil {
		avatarURL = getAvatarURLForUser(cli, userId)

		// TODO: Cache this if we ever react to nickname/profile picture updates
		base64Avatar := getBase64Avatar(cli, avatarURL)
		avatars[roomId+userId] = base64Avatar
	}

	// Get the channelId by checking whether the same message was just fired on one of
	// the channels our bot has been invited to
	fmt.Println("registrations:", registrations)
	channelId := ""
	for index, msg := range registrations {
		if msg.Content == "!play " + strings.Join(messageText, " ") {
			channelId = registrations[index].Channel
		}
	}

	if channelId == "" {
		fmt.Sprintln("Unable to find channel for registration:", strings.Join(messageText, " "))
	}

	// Create new player object and store in players map
	players[roomId+userId] = &Player{
		Stats: &Stats{
			Incontinence: 1,
		},
		MaxHold:   4,
		RoomId:    roomId,
		ChannelId: channelId,
		Name:      displayName,
		IsBig:     isbig,
		AvatarURL: avatarURL,
	}

	if isbig {
		return message("You are now playing as a big, ", displayName, ". Have fun!")
	}
	return message("You are now playing as a little, ", displayName, ". Have fun!")
}

// listPlayers will print out all known players in the game session
// TODO: List out in a vertical list
// TODO: Show whether players are paused with the unicode pause symbol
func listPlayers(roomId string) interface{} {
	var playernames []string
	for _, player := range players {
		if player.RoomId == roomId {
			if player.IsBig {
				playernames = append(playernames, player.Name+" (Big)")
			} else {
				playernames = append(playernames, player.Name+" (little)")
			}
		}
	}
	return strings.Join(playernames, ", ")

	/* TODO: Enable once the bridge doesn't parse names in code blocks
	fstr := "<p>Currently known players:</p><pre><code>"
	str := "Currently known players:\n```"
	first := true
	for key, player := range players {
		// Only show players for the current room
		if !strings.HasPrefix(key, roomId) {
			continue
		}

		playername := ""

		if player.IsBig {
			playername = player.Name + " (Big)"
		} else {
			playername = player.Name + " (little)"
		}

		// Eliminate extra spacing on Riot
		if first {
			fstr = fstr + playername
			first = false
		} else {
			fstr = fstr + "\n" + playername
		}
		str = str + "\n" + playername
	}
	fstr = fstr + "\n</code>"
	str = str + "\n```"
	return hmessage(str, fstr)
	*/
}

type Diaper struct {
	Thickness int    `json:"thickness"`
	StatMods  *Stats `json:"statmods"`
}

type Item struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Consumable  bool   `json:"consumable"`
	StatMods    *Stats `json:"statmods"`
}

// processItem will take a string and create a new item object from it.
// If a certain item attribute is not specified, it will be set to the default
func processItem(attrs []string) (newItem *Item) {
	newItem = &Item{
		StatMods: &Stats{},
	}

	// To Lower item name
	newItem.Name = attrs[0]

	// Iterate through each of the arguments and assign their given values
	for _, arg := range attrs[1:] {
		keyvalue := strings.Split(arg, ":")
		if len(keyvalue) == 1 {
			newItem.Description = arg
			continue
		}
		switch strings.ToLower(keyvalue[0]) {
		case "consumable", "cons":
			newItem.Consumable, _ = strconv.ParseBool(keyvalue[1])
		case "strength", "str":
			newItem.StatMods.Strength, _ = strconv.Atoi(keyvalue[1])
		case "constitution", "con":
			newItem.StatMods.Constitution, _ = strconv.Atoi(keyvalue[1])
		case "dexterity", "dex":
			newItem.StatMods.Dexterity, _ = strconv.Atoi(keyvalue[1])
		case "intelligence", "int":
			newItem.StatMods.Intelligence, _ = strconv.Atoi(keyvalue[1])
		case "wisdom", "wis":
			newItem.StatMods.Wisdom, _ = strconv.Atoi(keyvalue[1])
		case "charisma", "cha":
			newItem.StatMods.Charisma, _ = strconv.Atoi(keyvalue[1])
		case "regression", "reg":
			newItem.StatMods.Regression, _ = strconv.Atoi(keyvalue[1])
		case "incontinence", "inc":
			newItem.StatMods.Incontinence, _ = strconv.Atoi(keyvalue[1])
		case "bladder", "bla":
			newItem.StatMods.Bladder, _ = strconv.Atoi(keyvalue[1])
		case "bowels", "bow":
			newItem.StatMods.Bowels, _ = strconv.Atoi(keyvalue[1])
		}
	}

	return
}

// listItems will return a formatted message that lists all currently known
// items
func listItems(roomId string) interface{} {
	fstr := "<p>Currently known items:</p><pre><code>"
	str := "Currently known items:\n```"
	first := true
	for key, item := range items {
		// Only show items for the current room
		if !strings.HasPrefix(key, roomId) {
			continue
		}

		// Eliminate extra spacing on Riot
		if first {
			fstr = fstr + item.Name
			first = false
		} else {
			fstr = fstr + "\n" + item.Name
		}
		str = str + "\n" + item.Name
	}
	fstr = fstr + "\n</code>"
	str = str + "\n```"
	return hmessage(str, fstr)
}

func listCommands(commands []types.Command) interface{} {
	fstr := "<p>Commands:</p><pre><code>"
	str := "Commands:\n```"
	first := true
	for _, command := range commands {
		// Eliminate extra spacing on Riot
		commandText := strings.Join(command.Path, ", ")
		if first {
			fstr += commandText
			first = false
		} else {
			fstr += "\n" + commandText
		}
		str += "\n" + commandText
		str += "\n\t" + command.Help
		fstr += "\n\t" + command.Help

		str += "\n\t" + "Usage: " + command.Usage
		fstr += "\n\t" + "Usage: " + command.Usage
	}
	fstr += "\n</code>"
	str += "\n```"

	return hmessage(str, fstr)
}

// Commands
//   Currently supported:
//	 !echo
func (s *Service) Commands(cli *gomatrix.Client) []types.Command {
	return []types.Command{
		types.Command{
			Path:  []string{"echo"},
			Help:  "Make the bot say something!",
			Usage: "!echo I'm a silly butt!",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return message(strings.Join(args, " ")), nil
			},
		},
		types.Command{
			Path:  []string{"help"},
			Help:  "Print out this help text",
			Usage: "!help",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return listCommands(service.Commands(cli)), nil
			},
		},
		types.Command{
			Path:  []string{"play"},
			Help:  "Start playing!",
			Usage: "!play little|big",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				stateEvent, err := getStateEvent(cli, roomId, "m.room.member", userId)
				if err != nil {
					return message("Whoopsie! Error: ", err), nil
				}
				displayName := stateEvent["displayname"]
				return registerPlayer(cli, roomId, userId, displayName, args), nil
			},
		},
		types.Command{
			Path:  []string{"stopplaying"},
			Help:  "End your play session. WARNING: This will delete your player data!",
			Usage: "!stopplaying",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				delete(players, roomId+userId)
				return message(getStatus("player_stops_playing")), nil
			},
		},
		types.Command{
			Path:  []string{"pause"},
			Help:  "Pause your play session temporarily",
			Usage: "!pause",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				// Add player to paused_players map
				paused_players[roomId+userId] = players[roomId+userId]

				// Remove player from players slice by creating a new map
				// Without the old data we want to remove
				// Due to https://stackoverflow.com/a/23231539
				newMap := make(map[string]*Player)
				for key, value := range players {
					if key != roomId+userId {
						newMap[key] = value
					}
				}
				players = newMap;
				return message(getStatus("player_pause")), nil
			},
		},
		types.Command{
			Path:  []string{"unpause"},
			Help:  "Start playing again",
			Usage: "!unpause",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				// Add player back to players map
				players[roomId+userId] = paused_players[roomId+userId]

				// Remove player from paused_players slice by creating a new map
				// Without the old data we want to remove
				// Due to https://stackoverflow.com/a/23231539
				newMap := make(map[string]*Player)
				for key, value := range players {
					if key != roomId+userId {
						newMap[key] = value
					}
				}
				paused_players = newMap;
				return message(getStatus("player_unpause")), nil
			},
		},
		types.Command{
			Path:  []string{"listplayers"},
			Help:  "Lists all people currently playing",
			Usage: "!listplayers",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return message(listPlayers(roomId)), nil
			},
		},
		types.Command{
			Path:  []string{"creategroup"},
			Help:  "Lists all people currently playing",
			Usage: "!listplayers",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return listPlayers(roomId), nil
			},
		},
		/*
			// TODO: Make it a blacklist instead of a whitelist, so you can make it so other bad players can't influence your fun
			types.Command{
				Path:  []string{"addbig"},
				Help:  "Set who your Big is! Required for them to do some things to you.",
				Usage: "!addbig playername",
				Command: func(roomId, userId string, args []string) (interface{}, error) {
					if _, ok := players[roomId+userId]; !ok {
						return message(getStatus("player_not_playing")), nil
					}
					bigname := strings.Join(args, " ")
					big, exists := getPlayerByRoomAndName(roomId, bigname)
					if !exists {
						return message(big.Name, getStatus("big_not_playing")), nil
					}
					big.Littles = append(big.Littles, roomId+userId)
					return message(big.Name, getStatus("player_add_big")), nil
				},
			},
		*/
		types.Command{
			Path:  []string{"diaper"},
			Help:  "Wrap somepony up in a big, thick diaper! Bigs only!",
			Usage: "!diaper thickness (in cm) playername",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				if len(args) < 2 {
					return message("Syntax is !diaper thickness(cm) littlename"), nil
				}
				if !players[roomId+userId].IsBig {
					return message(getStatus("little_diaper")), nil
				}
				littlename := strings.Join(args[1:], " ")
				little, exists := getPlayerByRoomAndName(roomId, littlename)
				if !exists {
					return message(littlename, getStatus("big_not_playing")), nil
				}
				thickness, _ := strconv.ParseFloat(args[0], 64)
				roundedThickness := int(thickness)
				if thickness <= 0 {
					return message(getStatus("thickness_too_small")), nil
				}
				return little.Diaper(&Diaper{
					Thickness: roundedThickness,
				}), nil
			},
		},
		types.Command{
			Path:  []string{"feed"},
			Help:  "Make a player eat another item. Bigs only!",
			Usage: "!feed itemname to playername",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				if match, _ := regexp.MatchString(".+ to .+", strings.Join(args, " ")); !match {
					return message("Syntax is !feed itemname to littlename."), nil
				}

				// Find the index of "to" in the message
				index := -1
				for i := range args {
					if args[i] == "to" {
						index = i
						break
					}
				}

				// Grab the player name and item name
				itemname := strings.Join(args[:index], " ")
				playername := strings.Join(args[index+1:], " ")
				player, exists := getPlayerByRoomAndName(roomId, playername)
				if !exists {
					return message(playername, " is not playing yet!"), nil
				}
				if players[roomId+userId] == player {
					return message("Use !use to use an item on yourself!"), nil
				}

				// Consume the item
				if item, ok := items[roomId+strings.ToLower(itemname)]; ok {
					return player.ConsumeItem(item), nil
				}
				return message(itemname, " is not a valid item. Create one with !createitem."), nil
			},
		},
		types.Command{
			Path:  []string{"use"},
			Help:  "Use an item on yourself",
			Usage: "!use itemname",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				if len(args) == 0 {
					return message("Syntax is !use itemname"), nil
				}
				upperItemname := strings.Join(args, " ")
				itemname := strings.ToLower(upperItemname)
				player := players[roomId+userId]
				for _, item := range player.Items {
					if item == itemname {
						// Remove item from player's inventory
						player.RemoveItem(itemname)

						return player.ConsumeItem(items[roomId+itemname]), nil
					}
				}
				return message("Sorry, you don't seem to have a(n) ", upperItemname,
					" in your inventory."), nil
			},
		},
		types.Command{
			Path:  []string{"drop"},
			Help:  "Remove an item from your inventory",
			Usage: "!drop item name",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				player := players[roomId+userId]
				upperItemName := strings.Join(args, " ")
				lowerItemName := strings.ToLower(upperItemName)
				if _, exists := items[roomId+lowerItemName]; !exists {
					return message("I don't know what a ", upperItemName, " is! Check !listitems."), nil
				}

				foundItem := true
				for index, itemname := range player.Items {
					foundItem = true
					// Delete one instance of the item from the players inv.
					if itemname == lowerItemName {
						player.Items = append(player.Items[index:], player.Items[index+1:]...)
						break
					}
					foundItem = false
				}

				if foundItem {
					return message(getStatus("remove_item")), nil
				}
				return message(getStatus("player_not_have_item")), nil
			},
		},
		types.Command{
			Path:  []string{"listitems"},
			Help:  "List all the known items in this room",
			Usage: "!listitems",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return listItems(roomId), nil
			},
		},
		types.Command{
			Path:  []string{"give"},
			Help:  "Gives the user an item TODO: Make it so you can give items to other players",
			Usage: "!give item name",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				player := players[roomId+userId]
				upperItemName := strings.Join(args, " ")
				itemname := strings.ToLower(upperItemName)
				if _, ok := items[roomId+itemname]; !ok {
					return message("I don't know what a ", upperItemName, " is! Check !listitems."), nil
				}
				player.Items = append(player.Items, itemname)
				// TODO: make a status message (perhaps with %item% for substituting names
				return message("Here's a ", upperItemName, "!"), nil
			},
		},
		types.Command{
			Path:  []string{"createitem"},
			Help:  "Creates a new item with the given status effects",
			Usage: "!createitem \"Item Name\" \"It does stuff!\" consumable:true|false [stats...].",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if len(args) < 3 {
					return message("Syntax is !createitem \"Item Name\" \"It does stuff!\" consumable:true|false [stats...]."), nil
				}
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				for _, word := range args {
					if word == "to" {
						return message("Item name's can't contain the word \"to\", sorry technical reasons!"), nil
					}
				}
				newItem := processItem(args)
				if _, ok := items[roomId+strings.ToLower(newItem.Name)]; ok {
					return message(newItem.Name, "already exists! Use !edititem to edit."), nil
				}
				items[roomId+strings.ToLower(newItem.Name)] = newItem
				itemAttrs, _ := json.MarshalIndent(&newItem, "", " ")
				return message("Item", newItem.Name, getStatus("new_item_added"),
					"\nStats: \n", string(itemAttrs)), nil
			},
		},
		types.Command{
			// TODO: Add a drop (removes from user's inventory)
			Path:  []string{"edititem"},
			Help:  "Edit an existing item",
			Usage: "!edititem \"Item Name\" \"It does stuff!\" consumable:true|false [stats...].",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if len(args) < 2 {
					return message("Syntax is !edititem \"Item Name\" \"It does stuff!\" consumable:true|false [stats...]."), nil
				}
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				newItem := processItem(args)
				items[roomId+strings.ToLower(newItem.Name)] = newItem
				itemAttrs, _ := json.MarshalIndent(&newItem, "", " ")
				return message("Item ", newItem.Name, " has been edited!",
					"\nStats: \n", string(itemAttrs)), nil
			},
		},
		types.Command{
			Path:  []string{"removeitem"},
			Help:  "Remove an item from the room",
			Usage: "!removeitem item name",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				upperItemName := strings.Join(args, " ")
				itemname := strings.ToLower(upperItemName)
				if _, exists := items[roomId+itemname]; !exists {
					return message("I don't know what a ", upperItemName, " is! Check !listitems."), nil
				}
				delete(items, roomId+itemname)

				// Delete the item from everyone's list
				for _, player := range players {
					for index, itemId := range player.Items {
						// If the item is in the player's item list, remove it
						if itemId == itemname {
							player.Items = append(player.Items[index:], player.Items[index+1:]...)
							fmt.Println("Deleted, player items are now: ", player.Items)
						}
					}
				}
				return message(getStatus("remove_item")), nil
			},
		},
		types.Command{
			Path:  []string{"renameitem"},
			Help:  "Rename an item",
			Usage: "!renameitem Current Name to New Name",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				// Check syntax
				if match, _ := regexp.MatchString(".+ to .+", strings.Join(args, " ")); !match {
					return message("Syntax is !renameitem Current Name to New Name."), nil
				}

				// Find the index of "to" in the message
				index := -1
				for i := range args {
					if args[i] == "to" {
						index = i
						break
					}
				}

				// Grab the old and new item name
				oldName := strings.Join(args[:index], " ")
				newName := strings.Join(args[index+1:], " ")

				for _, word := range args[index+1:] {
					if word == "to" {
						return message("Item name's can't contain the word \"to\", sorry technical reasons!"), nil
					}
				}

				lowerOldName := strings.ToLower(oldName)
				lowerNewName := strings.ToLower(newName)
				if _, exists := items[roomId+lowerOldName]; !exists {
					return message("I don't know what a ", oldName, " is! Check !listitems."), nil
				}

				// Replace item's name with new name
				oldItem := *(items[roomId+lowerOldName])
				items[roomId+lowerNewName] = &oldItem
				items[roomId+lowerNewName].Name = newName

				// If item's lowercase name is different,
				// delete old item
				if lowerOldName != lowerNewName {
					delete(items, roomId+lowerOldName)
				}

				// Rename the item in everyone's inventory
				for _, player := range players {
					for index, itemId := range player.Items {
						// If the item is in the player's item list, remove it
						if itemId == lowerOldName {
							player.Items[index] = lowerNewName
						}
					}
				}

				return message("Item", oldName, "has been renamed to", newName), nil
			},
		},
		types.Command{
			Path:  []string{"myitems"},
			Help:  "List your current items",
			Usage: "!myitems",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return players[roomId+userId].ListItems(), nil
			},
		},
		types.Command{
			Path:  []string{"change"},
			Help:  "Change a player or yourself",
			Usage: "!change newthickness (in cm) playername OR !change newthickness",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				playerName := strings.Join(args[1:], " ")
				newThickness, _ := strconv.Atoi(args[0])
				if playerName == "" {
					return players[roomId+userId].Change(newThickness), nil
				}
				if player, exists := getPlayerByRoomAndName(roomId, playerName); exists {
					return player.Change(newThickness), nil
				}
				return message("I don't know who ", playerName, " is!"), nil
			},
		},
		types.Command{
			Path:  []string{"stats"},
			Help:  "Show information about yours or someone else's stats",
			Usage: "!stats [playername]",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if len(args) == 0 {
					player := players[roomId+userId]
					stats, _ := json.MarshalIndent(player, "", " ")
					return message(string(stats)), nil
				}
				playerName := strings.Join(args, " ")
				player, exists := getPlayerByRoomAndName(roomId, playerName)
				if !exists {
					return message("I don't know who ", playerName, " is!"), nil
				}
				stats, _ := json.MarshalIndent(player, "", " ")
				return message(string(stats)), nil
			},
		},
		types.Command{
			Path:  []string{"release"},
			Help:  "Just let go...",
			Usage: "!release",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if player, ok := players[roomId+userId]; ok {
					if player.Stats.Bladder == 0 && player.Stats.Bowels == 0 {
						return message(getStatus("player_bladder_bowels_empty")), nil
					}
					player.WetDiaper()
					player.MessDiaper()
					return message(player.Name, getStatus("player_wet_mess")), nil
				}
				return message(getStatus("player_not_playing")), nil
			},
		},
		types.Command{
			Path:  []string{"hold"},
			Help:  "Try to hold it before using the restroom!",
			Usage: "!hold",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if player, ok := players[roomId+userId]; ok {
					rand.Seed(time.Now().Unix())
					randNum := rand.Intn(100)
					// Hold depending on random number
					if randNum > player.Stats.Incontinence {
						if player.HoldCount > 0 {
							return message(getStatus("already_holding")), nil
						}
						if player.MaxHold == 0 {
							return message(getStatus("out_of_holds")), nil
						}
						player.Hold()
						return message(getStatus("hold_success"), " Holding for ",
							player.HoldCount, " turns."), nil
					} else {
						player.MaxHold = 0
						return message(getStatus("hold_fail")), nil
					}
				}
				return message(getStatus("player_not_playing")), nil
			},
		},
		types.Command{
			Path:  []string{"togglemute"},
			Help:  "TODO: Temporary command. Mute/Unmute yourself",
			Usage: "!togglemute",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				player := players[roomId+userId]
				if player.IsMuted {
					player.IsMuted = false
					return message("You are no longer muted!"), nil
				}

				player.IsMuted = true
				return message("You're now as silent as a lamb~"), nil
			},
		},
	}
}

// Expansions
// Currently used to run a method every time someone talks
func (s *Service) Expansions(cli *gomatrix.Client) []types.Expansion {
	return []types.Expansion{
		types.Expansion{
			Regexp: regexp.MustCompile(`.*`),
			Expand: func(roomId, userId, eventID string, messageText []string) interface{} {
				return onMessage(cli, roomId, userId, eventID, messageText)
			},
		},
	}
}

// message wraps a string in a m.notice gomatrix.TextMessage pointer that can
// be returned to gomatrix to send to the room
func message(text ...interface{}) interface{} {
	return &gomatrix.TextMessage{
		"m.notice",
		strings.TrimSpace(fmt.Sprint(text...)),
	}
}

// hmessage wraps a string in a m.notice gomatrix.HTMLMessage pointer that can
// be returned to gomatrix to send markdown messages to the room
func hmessage(body, fbody string) interface{} {
	return &gomatrix.HTMLMessage{
		body,
		"m.notice",
		"org.matrix.custom.html",
		fbody,
	}
}

func getStateEvent(cli *gomatrix.Client, roomId, eventType, stateKey string) (stateEvent map[string]string, err error) {
	err = cli.StateEvent(roomId, eventType, stateKey, &stateEvent)
	return
}

func convertSpeechToSilence(input string) string {
	return "Mmmph!" + "\n"
}

// convertSpeechToLisp changes all r's and l's to w's
func convertSpeechToLisp(input string) string {
	replacementMap := make(map[string]string)

	// Iterate through string and change words
	words := strings.Fields(input)
	for _, word := range words {
		if replacement, ok := replacementMap[strings.ToLower(word)]; ok {
			// Replace word with replacement in string
			if word == strings.ToUpper(word) { // Is string all uppercase?
				input = strings.Replace(input, word, strings.ToUpper(replacement), 1)
			} else if string(word[0]) == strings.ToUpper(string(word[0])) { // First letter uppercase
				input = strings.Replace(input, word, strings.Title(replacement), 1)
			} else { // Just lowercase
				input = strings.Replace(input, word, replacement, 1)
			}
		}
	}

	input = strings.Replace(input, "r", "w", -1)
	input = strings.Replace(input, "l", "w", -1)
	return input + "\n"
}

// onMessage runs each time a user posts a message in the channel
func onMessage(cli *gomatrix.Client, roomId, userId, eventId string, messageText []string) interface{} {
	returnMessage := ""

	// Don't do anything if they aren't playing
	player, ok := players[roomId+userId]
	if !ok {
		return nil
	}

	// Increase bladder and bowels every turn
	// TODO: Change to every new time someone talks
	ret := player.increaseBladderBowels()
	editedMessage := ""
	if ret != nil {
		editedMessage += ret.(string)
	}

	// Change player's messages
	msgText := strings.Join(messageText, " ")
	if player.Stats.Regression > 80 {
		editedMessage += "\n" + convertSpeechToLisp(msgText)
	}
	if player.IsMuted {
		editedMessage += "\n" + convertSpeechToSilence(msgText)
	}

	if editedMessage != "" {
		// Delete original message
		_, err := cli.RedactEvent(roomId, eventId, &gomatrix.ReqRedact{Reason: "Edited by diaper roleplay bot"})

		if err != nil {
			editedMessage += err.Error()
		}

		// Do discord-specific code if enabled
		if discord != nil {
			// Send a webhook to discord to look like user's saying it
			//webhook, err := discord.WebhookCreate(player.ChannelId, player.Name, avatars[roomId+userId])
			webhooks, err := discord.ChannelWebhooks(player.ChannelId)
			if err != nil {
				fmt.Println("Error retrieving webhooks:", err)
			} else {
				// Get the webhook we want
				var webhook *discordgo.Webhook
				for _, wh := range webhooks {
					if wh.Name == "diaper-dnd" {
						webhook = wh
					}
				}

				if webhook == nil {
					fmt.Println("Unable to find channel webhook. Adding automatically...")
					returnMessage += "Make sure to add \"diaper-dnd\" webhook to the channel!\n"
				}
				data := &discordgo.WebhookParams{
					Content: editedMessage,
					Username: player.Name,
					AvatarURL: player.AvatarURL,
				}

				// Execute the discord webhook
				errExecute := discord.WebhookExecute(webhook.ID, webhook.Token, false, data)

				if errExecute != nil {
					fmt.Println("Error executing webhook:", errExecute)
				}

				// Remove original message from discord
				// TODO: MsgText is different if there's a newline in the message
				// TODO: Don't foalify image posts (or links)
				// TODO: DOn't foalify mentions
				if msg, ok := discordMessages[msgText]; ok {
					errMsgRemove := discord.ChannelMessageDelete(msg.ChannelID, msg.ID)

					if errMsgRemove != nil {
						fmt.Println("Error removing discord message:", errMsgRemove)
					}
				}
			}
		} else {
			returnMessage += editedMessage
		}
	}

	if returnMessage == "" {
		return nil
	}

	// If discord is enabled, send a messsage to matrix but not Discord by eliminating the formatted body
	if discord != nil {
		return hmessage(returnMessage, "")
	}
	return message(returnMessage)
}

// JSON

// retrieveStoredData parses the userdata.json file for the bot's state since last run
func retrieveStoredData(storageFolder string) {
	playersFile := fmt.Sprint(storageFolder, "/playerdata.json")
	statusFile := fmt.Sprint(storageFolder, "/statusmsgs.json")
	itemsFile := fmt.Sprint(storageFolder, "/items.json")
	wrdRplFile := fmt.Sprint(storageFolder, "/replacements.json")

	// Get players
	// Return if file does not exist yet (will be created on shutdown)
	if exists, _ := exists(playersFile); exists {

		raw, err := ioutil.ReadFile(playersFile)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		err = json.Unmarshal(raw, &players)
		if err != nil {
			fmt.Println("Unable to read player data: ", err)
		}
	}

	// Get items
	// Return if file does not exist yet (will be created on shutdown)
	if exists, _ := exists(itemsFile); exists {

		raw, err := ioutil.ReadFile(itemsFile)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		err = json.Unmarshal(raw, &items)
		if err != nil {
			fmt.Println("Unable to read item data: ", err)
		}
	}

	// Get status messages
	// Return if file does not exist yet (will be created on shutdown)
	if exists, _ := exists(statusFile); exists {

		raw, err := ioutil.ReadFile(statusFile)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		err = json.Unmarshal(raw, &statusMsgs)
		if err != nil {
			fmt.Println("Unable to read status message data: ", err)
		}
	}

	// Get word replacement files (for use with baby talk)
	// Return if file does not exist yet (will be created on shutdown)
	if exists, _ := exists(wrdRplFile); exists {

		raw, err := ioutil.ReadFile(wrdRplFile)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		err = json.Unmarshal(raw, &wordReplacements)
		if err != nil {
			fmt.Println("Unable to read word replacement data: ", err)
		}

		fmt.Println("Got the following: ", wordReplacements)
	}

	// Initialize the data maps if not already
	if players == nil {
		players = make(map[string]*Player)
	}
	if paused_players == nil {
		paused_players = make(map[string]*Player)
	}
	if items == nil {
		items = make(map[string]*Item)
	}
	if avatars == nil {
		avatars = make(map[string]string)
	}
	if statusMsgs == nil {
		statusMsgs = make(map[string]*StatusMsg)
	}
	if discordMessages == nil {
		discordMessages = make(map[string]*discordgo.Message)
	}
	if wordReplacements == nil {
		wordReplacements = make(map[string][]string)
	}
}

func storeData(storageFolder string) {
	playersFile := fmt.Sprint(storageFolder, "/playerdata.json")
	itemsFile := fmt.Sprint(storageFolder, "/items.json")
	avatarFile := fmt.Sprint(storageFolder, "/avatars.json")
	regFile := fmt.Sprint(storageFolder, "/avatars.json")
	wrdRplFile := fmt.Sprint(storageFolder, "/replacements.json")

	// Backup player and item data
	os.Link(playersFile, playersFile+".backup")
	os.Link(itemsFile, itemsFile+".backup")
	os.Link(avatarFile, avatarFile+".backup")
	os.Link(regFile, regFile+".backup")
	os.Link(wrdRplFile, wrdRplFile+".backup")

	// Store player information
	playerdata, _ := json.MarshalIndent(&players, "", " ")

	err := ioutil.WriteFile(playersFile, playerdata, 0644)
	if err != nil {
		fmt.Println("Error writing player data: ", err)
	}

	// Store item information
	itemdata, _ := json.MarshalIndent(&items, "", " ")

	err = ioutil.WriteFile(itemsFile, itemdata, 0644)
	if err != nil {
		fmt.Println("Error writing item data: ", err)
	}

	// Store avatar information
	avatardata, _ := json.MarshalIndent(&avatars, "", " ")

	err = ioutil.WriteFile(avatarFile, avatardata, 0644)
	if err != nil {
		fmt.Println("Error writing avatar data: ", err)
	}

	// Store registration information
	regdata, _ := json.MarshalIndent(&registrations, "", " ")

	err = ioutil.WriteFile(regFile, regdata, 0644)
	if err != nil {
		fmt.Println("Error writing registrations data: ", err)
	}

	// Store registration information
	rpldata, _ := json.MarshalIndent(&wordReplacements, "", " ")

	err = ioutil.WriteFile(wrdRplFile, rpldata, 0644)
	if err != nil {
		fmt.Println("Error writing word replacement data: ", err)
	}
}

// exists returns whether the given file or directory exists or not
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// getBase64Avatar will, given a userId, return a b64 representation of their avatar
// Used for generating Discord webhooks
func getBase64Avatar(cli *gomatrix.Client, avatarURL string) string {
	resp, err := http.Get(avatarURL)
	if err != nil {
		fmt.Println("Error retrieving avatar URL:", err)
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	img, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading the response, ", err)
		return ""
	}

	return base64.StdEncoding.EncodeToString(img)
}

// getAvatarURLForUser will parse the mxc:// url provided by gomatrix and retrieve
// a http URL used for grabbing the user's actual avatar
func getAvatarURLForUser(cli *gomatrix.Client, userId string) string {
	avatarURL, err := cli.GetAvatarURLForUser(userId)

	if err != nil {
		fmt.Sprintln("Error getting Avatar URL for", userId, ", err:", err)
		return "error"
	}

	mediaIndex := 6
	for index, character := range avatarURL[6:] {
		if character == '/' {
			mediaIndex += index
			break
		}
	}

	return hsURL + "/_matrix/media/r0/download/" + avatarURL[6:mediaIndex] + avatarURL[mediaIndex:]
}

// onDiscordMessage will be fired whenever a message is created in a Discord channel we're invited to
// TODO: Move to discord.go or something
func onDiscordMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Keep track of the last five !play announcements
	// Check if the message starts with !play
	// Indicates a player is starting play
	if strings.HasPrefix(m.Content, "!play") {
		// Find the channel that the message came from.
		c, err := s.State.Channel(m.ChannelID)
		if err != nil {
			// Could not find channel.
			return
		}

		// Trim slice if necessary
		for len(registrations) >= 5 {
			// Knock off first item of registrations
			registrations = append(registrations[:0], registrations[1:]...)
		}

		// Record channelId for each message
		registrations = append(registrations, &DiscordMessage{Content: m.Content, Channel: c.ID})
	}

	// Add discordmessage to record
	discordMessages[m.Content] = m.Message
}

func init() {
	storageFolder := "src/github.com/matrix-org/go-neb/services/dndrp"

	flag.StringVar(&token, "t", "", "Bot token")
	flag.StringVar(&hsURL, "h", "", "Home server URL (ex: https://example.url")
	flag.Parse()

	// Set up Discord-specific component of bot
	if token != "" && hsURL != "" {
		discordSession, err := discordgo.New("Bot " + token)
		if err != nil {
			fmt.Println("Error creating discord session:", err)
		}
		discord = discordSession

		// Register messageCreate as a callback for the messageCreate events.
		discord.AddHandler(onDiscordMessage)

		// Open the websocket and begin listening.
		err = discord.Open()
		if err != nil {
			fmt.Println("Error opening Discord session: ", err)
		}
	} else {
		fmt.Println("WARNING: Running without Discord-specific code. Add -t bot_token and -h HS URL to end of command to enable")
	}

	types.RegisterService(func(serviceId, serviceUserId, webhookEndpointURL string) types.Service {
		service = &Service{
			DefaultService: types.NewDefaultService(serviceId, serviceUserId, ServiceType),
		}
		return service
	})

	// Write data on bot shutdown
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		// TODO: Ask neb developers if this is a clean shutdown...
		fmt.Println("Shutting down cleanly...")
		storeData(storageFolder)
		os.Exit(1)
	}()

	retrieveStoredData(storageFolder)
}
